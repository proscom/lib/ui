import isNil from 'lodash/isNil';

/**
 * Применяет значения по-умолчанию для объекта
 * @param object - объект
 * @param defaults - значения по-умолчанию
 */
export function applyDefaults<
  T extends { [key: string]: any }
>(object: T, defaults: Partial<T>): T {
  const result: T = { ...object };
  const keys = Object.keys(object) as (keyof T)[];
  for (let key of keys) {
    if (isNil(result[key]) && key in defaults) {
      // @ts-ignore
      result[key] = defaults[key];
    }
  }

  return result;
}
