/**
 * Создаёт функцию, которая проверяет, что переданный элемент
 * подходит под поисковый запрос
 *
 * @param query - поисковый запрос
 * @example
 *  const filter = createFilterSearch('a b');
 *  filter('abc') -> false
 *  filter('aec bcd') -> true
 *  filter('bce daf') -> false
 */
export function createFilterSearch(query: string | null | undefined) {
  if (!query) return () => true;
  const keywords = query
    .toLowerCase()
    .split(' ')
    .filter((x) => x);
  if (keywords.length === 0) return () => true;
  return (item: string) => {
    const itemLower = item.toLowerCase();
    let index = 0;
    let newIndex;
    let find = false;
    for (const keyword of keywords) {
      newIndex = itemLower.indexOf(keyword, index);
      if (newIndex >= index) {
        index = newIndex;
        find = true;
      } else {
        find = false;
        break;
      }
    }
    return find;
  };
}

/**
 * Те ключи объекта Obj, которые имеют строки в качестве значения
 */
export type StringKeysOf<Obj extends {}> = {
  [Key in keyof Obj]: Obj[Key] extends string ? Key : never;
}[keyof Obj];

/**
 * Фильтрует массив по поисковому запросу с учетом порядка слов
 *
 * @param query - поисковый запрос
 * @param array - массив который фильтруем
 * @param field - свойство массива с именем
 */
export function filterSearch<T extends string | object>(
  query: string | null | undefined,
  array: T[] | null | undefined,
  field?: StringKeysOf<T> | ((obj: T) => string)
): T[] {
  if (!query) return array ? array : [];
  if (!array) return [];

  const checkItem = createFilterSearch(query);
  return array.filter((object) => {
    let item;
    if (field) {
      if (typeof field === 'string') {
        item = object[field];
      } else if (typeof field === 'function') {
        item = field(object);
      } else {
        throw new Error(
          `Argument 'field' should be either string or function. Got ${typeof field}`
        );
      }
    } else {
      item = object;
    }
    return checkItem(item);
  });
}
