import { Extends } from '../types';

export type EnumValueInfo = {
  name: string;
};

export type StringEnum = {
  [key: string]: string;
};

export type EnumKey<T> = T[keyof T];

export type Option<
  LabelType,
  OptionType,
  LabelKey extends string = 'label',
  ValueKey extends string = 'value'
> = {
  [k in LabelKey]: LabelType;
} &
  {
    [k in ValueKey]: OptionType;
  };

export type EnumOption<
  ValueType,
  ValueInfo,
  LabelType = string,
  LabelKey extends string = 'label',
  ValueKey extends string = 'value'
> = Extends<
  Option<LabelType, EnumKey<ValueType>, LabelKey, ValueKey>,
  ValueInfo
>;

/**
 * Набор утилит для работы с энумами (перечислениями).
 * Решает частые задачи, такие как формирование массивов опций для селектов,
 * поиск ключа по имени.
 *
 * @example
 * enum RoomType {
 *   Corridor = 'corridor',
 *   Class = 'class',
 *   Restroom = 'restroom',
 * }
 *
 * // Надо явно указать аргументы дженерика, потому что TS (v4.1) недостаточно умён
 * const roomType = new EnumHelpers<typeof RoomType, { name: string, order: number }>(
 *   RoomType,
 *   {
 *     [RoomType.Corridor]: { name: 'Коридор', order: 1 },
 *     [RoomType.Class]: { name: 'Аудитория', order: 2 },
 *     [RoomType.Restroom]: { name: 'Комната отдыха', order: 3 }
 *   }
 * );
 *
 * const roomTypes = roomType.getOptions().sort((a,b) => a.order - b.order);
 */
export class EnumHelpers<
  ValueType extends StringEnum,
  ValueInfo extends EnumValueInfo
> {
  /**
   * Конструктор
   * @param values - энум (enum), содержащее только ключи
   * @param info - дополнительные данные, на каждый ключ энума
   *  содержит объект, в том числе содержащий имя в поле name
   */
  constructor(
    public values: ValueType,
    public info: { [key in EnumKey<ValueType>]: ValueInfo }
  ) {}

  /**
   * Возвращает массив ключей энума
   */
  keys() {
    return Object.values(this.values) as EnumKey<ValueType>[];
  }

  /**
   * Возвращает массив опций энума.
   * Подходит для селектов, табов и т.п.
   */
  getOptions(): Array<EnumOption<ValueType, ValueInfo>>;
  /**
   * Возвращает массив опций энума.
   * Подходит для селектов, табов и т.п.
   *
   * @field - поле, которое возвращается в качестве label
   */
  getOptions<Field extends keyof ValueInfo>(
    field: Field
  ): Array<EnumOption<ValueType, ValueInfo, ValueInfo[Field]>>;
  /**
   * Возвращает массив опций энума.
   * Подходит для селектов, табов и т.п.
   *
   * @field - поле, которое возвращается в качестве label
   * @labelKey - ключ, в который записывается label
   * @valueKey - ключ, в который записывается ключ энума
   */
  getOptions<
    Field extends keyof ValueInfo,
    Label extends string,
    Value extends string
  >(
    field: Field,
    labelKey: Label,
    valueKey: Value
  ): Array<EnumOption<ValueType, ValueInfo, ValueInfo[Field], Label, Value>>;
  /**
   * Возвращает массив опций энума.
   * Подходит для селектов, табов и т.п.
   *
   * @field - поле, которое возвращается в качестве label
   * @labelKey - ключ, в который записывается label
   * @valueKey - ключ, в который записывается ключ энума
   */
  getOptions<
    Field extends keyof ValueInfo,
    Label extends string = 'label',
    Value extends string = 'value'
  >(
    field: 'name' | Field = 'name',
    labelKey: 'label' | Label = 'label',
    valueKey: 'value' | Value = 'value'
  ):
    | Array<EnumOption<ValueType, ValueInfo>>
    | Array<EnumOption<ValueType, ValueInfo, ValueInfo[Field]>>
    | Array<EnumOption<ValueType, ValueInfo, ValueInfo[Field], Label, Value>> {
    return Object.keys(this.values).map((key: EnumKey<ValueType>) => {
      const info = this.info[key] as ValueInfo;
      return {
        ...info,
        [labelKey]: info[field],
        [valueKey]: this.values[key]
      };
    }) as any;
  }

  /**
   * Возвращает имя по ключу
   * @param value - ключ энума
   */
  getName(value: EnumKey<ValueType>) {
    return this.info[value]?.name;
  }

  /**
   * Возвращает ключ по имени
   * @param name - имя
   */
  getByName(name: string) {
    for (const key of Object.keys(this.info)) {
      if (this.info[key].name === name) {
        return key as EnumKey<ValueType>;
      }
    }
    return undefined;
  }

  /**
   * Возвращает ключ по произвольному значению
   * @param value - имя
   * @param field - поле, с которым сравнивается значение
   */
  getBy<Field extends keyof ValueInfo>(value: string, field: Field) {
    for (const key of Object.keys(this.info)) {
      if (this.info[key][field] === value) {
        return key as EnumKey<ValueType>;
      }
    }
    return undefined;
  }
}
