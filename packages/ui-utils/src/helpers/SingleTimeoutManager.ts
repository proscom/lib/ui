/**
 * Класс, управляющий одним таймаутом.
 * Удобен в случаях, когда при назначении нового таймаута старый необходимо отменить,
 * либо когда таймаут должен быть отменен при наступлении внешнего события
 * независимо от того, установлен он или нет.
 *
 * По сути делает таймаут локальным, а не глобальным.
 *
 * Может использоваться с React-компонентами через хук useTimeoutRef.
 */
export class SingleTimeoutManager {
  /**
   * Идентификатор активного таймаута.
   * Равен null, если таймаут не запущен или уже сработал.
   */
  timeout: any = null;

  /**
   * Запускает таймаут, отменяя предыдущий.
   *
   * @param cb - колбек, который будет вызван не раньше, чем через время time
   * @param time - время в мс, через которое будет вызван cb
   */
  public set(cb: (() => void) | null | undefined, time: number) {
    if (this.timeout) clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.timeout = null;
      cb?.();
    }, time);
  }

  /**
   * Создает таймаут-пустышку.
   *
   * @param time - время в мс, через которое таймаут будет снят
   */
  public setDelay(time: number) {
    return this.set(null, time);
  }

  /**
   * Если таймаут запущен, то отменяет его.
   */
  public clear() {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
  }

  /**
   * Возвращает идентификатор активного таймаута
   */
  public active(): boolean {
    return !!this.timeout;
  }
}
