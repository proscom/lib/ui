import { EMDASH } from '../utf';

const declinationCases = [2, 0, 1, 1, 1, 2] as const;

/**
 * Функция возвращает текст, соответствующий склонению числа в русской локали.
 * Строка может содержать специальный символ $n, заменяемый на число
 * @param number - число
 * @param single - формат строки для чисел, заканчивающихся на 1
 * @param special - формат строки для чисел, заканчивающихся на 2, 3, 4
 * @param plural - формат строки для всех остальных + исключения (...11, ...12, ...13, ...14)
 * @param none - формат строки, если number = null или undefined
 * @returns {string}
 * @example
 *  ```
 *  declinedText(2, 'Выбрана $n организация', 'Выбрано $n организации', 'Выбрано $n организаций') -> 'Выбрано 2 организации'
 *  ```
 */
export function declinedText(number: number|null|undefined, single: string, special: string, plural: string, none: string = EMDASH) {
  if (number === null || number === undefined) return none;
  const text = [single, special, plural][
    number % 100 > 4 && number % 100 < 20
      ? 2
      : declinationCases[number % 10 < 5 ? number % 10 : 5]
  ];
  return text.replace(/\$n/g, number.toString());
}
