/**
 * Создает конструктор опций для селекта
 * @param labelFn - функция получения названия
 * @param valueFn - функция получения значения
 */
export function makeOptionator<Value, RLabel, RValue = Value>(
  labelFn: (value: Value) => RLabel,
  valueFn?: (value: Value) => RValue
) {
  return (value: Value) => ({
    value: valueFn ? valueFn(value) : value,
    label: labelFn(value)
  });
}
