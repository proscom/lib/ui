import { HELLIP } from '../utf';

/**
 * Обрезает текст до заданного числа символов +- fuzziness
 * Старается обрезать по пробелу
 *
 * @param text
 * @param maxLength
 * @param fuzziness - количество символов вокруг maxLength, где искать пробел
 */
export function shortenText(text: string, maxLength?: number, fuzziness?: number): string;
export function shortenText(text: null, maxLength?: number, fuzziness?: number): null;
export function shortenText(text: undefined, maxLength?: number, fuzziness?: number): undefined;
export function shortenText(text: string|null|undefined, maxLength = 30, fuzziness = 5) {
  if (!text) {
    return text;
  }
  const textString = text + '';
  if (textString.length > maxLength) {
    let spaceIndex = -1;
    // Находим самый левый из группы самых правых пробельных символов
    for (let i = maxLength - 1; i >= maxLength - fuzziness; i--) {
      if (/\s/.test(textString[i])) {
        spaceIndex = i;
      } else if (spaceIndex !== -1) {
        break;
      }
    }
    if (spaceIndex >= 0) {
      return textString.slice(0, spaceIndex) + HELLIP;
    }
    return textString.slice(0, maxLength) + HELLIP;
  }
  return textString;
}
