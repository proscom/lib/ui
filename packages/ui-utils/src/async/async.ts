import {
  asyncify,
  eachOfLimit as eachOfLimitAsync,
  IterableCollection,
  mapLimit as mapLimitAsync,
} from 'async';
import chunk from 'lodash/chunk';
import concat from 'lodash/concat';

/**
 * Для каждого элемента array параллельно вызывает асинхронную функцию cb,
 * но не более limit одновременных вызовов.
 *
 * @param array - массив
 * @param limit - количество одновременно выполняемых операций cb
 * @param cb - колбек, принимающий элемент массива и его индекс
 */
export function eachOfLimit<T>(
  array: IterableCollection<T>,
  limit: number,
  cb: (item: T, index: number) => Promise<void>,
): Promise<void> {
  return eachOfLimitAsync(array, limit, asyncify(cb));
}

/**
 * Для каждого элемента array параллельно вызывает асинхронную функцию cb,
 * но не более limit одновременных вызовов.
 *
 * @param array - массив
 * @param limit - количество одновременно выполняемых операций cb
 * @param cb - колбек, принимающий элемент массива и его индекс и возвращающий результат
 * @returns result - массив результатов вызовов cb
 */
export function mapLimit<T, R>(
  array: IterableCollection<T>,
  limit: number,
  cb: (item: T, index: number) => Promise<R>,
): Promise<R[]> {
  return mapLimitAsync<T, R, Error>(array, limit, asyncify(cb));
}

/**
 * Для каждого элемента array параллельно вызывает асинхронную функцию cb,
 * но не более limit одновременных вызовов.
 *
 * Предполагается, что функция cb возвращает массив.
 * Конкатенирует результирующие массивы в один большой.
 *
 * @param array - массив
 * @param limit - количество одновременно выполняемых операций cb
 * @param cb - колбек, принимающий элемент массива и его индекс и возвращающий массив
 * @returns result - конкатенация результатов вызовов cb для каждого элемента array
 */
export async function concatLimit<T, R>(
  array: IterableCollection<T>,
  limit: number,
  cb: (item: T, index: number) => Promise<R[]>,
): Promise<R[]> {
  const items = await mapLimit(array, limit, cb);
  return concat([], ...items);
}

/**
 * Разбивает массив array на чанки размера chunkSize и выполняет для каждого чанка
 * асинхронную функцию cb параллельно с ограничением в callLimit одновременных вызовов.
 *
 * @param array
 * @param chunkSize
 * @param callLimit
 * @param cb
 */
export async function awaitChunkedPromises<T, R>(
  array: T[],
  chunkSize: number,
  callLimit: number,
  cb: (slice: T[], iSlice: number, nChunks: number) => Promise<R>,
): Promise<R[]> {
  const chunks = chunk(array, chunkSize);
  const nChunks = chunks.length;
  return await mapLimit(chunks, callLimit, (slice: T[], iSlice: number) => {
    return cb(slice, iSlice, nChunks);
  });
}
