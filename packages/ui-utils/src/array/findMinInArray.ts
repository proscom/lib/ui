import { findMinIndex } from './findMinIndex';

/**
 * Находит элемент массива, имеющий наименьшую метрику
 * @param array - массив для поиска
 * @param metric - функция, рассчитывающая метрику
 * @returns element - элемент массива, имеющий наименьшую метрику или undefined, если массив пустой
 */
export function findMinInArray<T>(array: T[], metric: (item: T, idx: number) => number) {
  const index = findMinIndex(array, metric);
  return index >= 0 ? array[index] : undefined;
}
