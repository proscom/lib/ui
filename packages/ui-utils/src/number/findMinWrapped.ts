import { findMinIndex } from '../array';

/**
 * Находит ближайшую к текущему значение вариацию следующего значения на замкнутом интервале
 *
 * @param current - текущее значение
 * @param next - следующее значение
 * @param wrap - период замкнутого интервала
 * @returns Одно из значений вида `next +- n * wrap`, которое ближе всего к `current` с учетом замкнутости
 * @example
 *  Эта функция может быть полезна для формирования плавных анимаций,
 *  например между углами:
 *
 *  findMinWrapped(15, 350, 360) -> -10
 *
 *  Угол -10 градусов равен углу 350 градусов. Периодичность градусов - 360.
 *  Но анимация от 15 до -10 получится более корректный, чем от 15 до 350.
 */
export function findMinWrapped(current: number, next: number, wrap: number) {
  const shift = wrap * Math.floor(current / wrap);
  const options = [shift + next, shift - wrap + next, shift + wrap + next];
  const minIndex = findMinIndex(options, (v) => Math.abs(v - current));
  return options[minIndex];
}
