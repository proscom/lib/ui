/**
 * Проверяет, что значение является валидным числом (не NaN и не Infinity)
 * @param n - значение для проверки
 */
export function isValidNumber(n: any) {
  return typeof n == 'number' && !isNaN(n) && isFinite(n);
}

export function tryNumber(value: any): number | null;
export function tryNumber<D>(
  value: any,
  defaultValue: D
): number | (D extends undefined ? null : D);
/**
 * Пытается представить value как валидное число и возвращает его в случае успеха.
 * В случае неудачи возвращает null
 *
 * @param value - значение для проверки
 * @param defaultValue - значение по-умолчанию
 */
export function tryNumber<D>(value: any, defaultValue?: D): number | D | null {
  if (value === undefined || value === null || value === '') {
    return defaultValue ?? null;
  }
  const number = +value;
  if (isValidNumber(number)) {
    return number;
  }
  return defaultValue ?? null;
}
