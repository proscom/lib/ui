import { formatNumber, formatValueUnit, Unit } from './formatNumber';

export type Decimals<Value> = number | ((value: Value) => number);

export type Index<Value> = { decimals: Decimals<Value> };

export function getIndexDecimals<Value extends any = any>(
  index: Index<Value>,
  value: Value
): number {
  if (typeof index.decimals === 'function') {
    return index.decimals(value);
  }
  return index.decimals;
}

export function formatIndexValue(value: number, index: Index<number>) {
  return formatNumber(value, getIndexDecimals(index, value));
}

export function formatIndexValueUnit(
  value: number,
  index: Index<number> & { unit: Unit }
) {
  return formatValueUnit(value, index.unit, getIndexDecimals(index, value));
}
