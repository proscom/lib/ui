/**
 * Возвращает красивое круглое число, не сильно больше переданного
 * @param number {number}
 * @returns {number}
 */
export function roundToRound(number: number) {
  if (!number) return number;
  const log10 = Math.log10(number);
  const floor = Math.floor(log10);
  const ceil = Math.ceil(log10);
  const fl10 = Math.pow(10, floor);
  const steps = number / fl10;
  if (steps >= 8) {
    return Math.pow(10, ceil);
  }
  if (steps <= 3) {
    return (Math.ceil(steps * 2) * fl10) / 2;
  }
  if (steps <= 1) {
    return (Math.ceil(steps * 10) * fl10) / 10;
  }
  return Math.ceil(steps) * fl10;
}

/**
 * Подбирает количество шагов легенды, чтобы цифры были красивые
 * @param max {number} - верхняя граница
 * @param min {number} - нижняя граница
 * @param decimals {number} - количество точек после запятой
 * @param maxSteps {number} - максимум шагов
 * @param minSteps {number} - миниммум шагов
 * @returns {number}
 */
export function getRoundLegendSteps(
  max: number,
  min = 0,
  decimals = 0,
  maxSteps = 11,
  minSteps = 5
) {
  let maxRounds = 0;
  let maxRoundsSteps = minSteps;
  for (let steps = minSteps; steps <= maxSteps; steps++) {
    const step = (max - min) / (steps - 1);

    let nRounds = 0;
    let value = min;
    for (let i = 0; i <= steps; i++) {
      const label = value.toFixed(decimals).toString();
      let zeros = 0;
      for (let k = label.length - 1; k >= 0; k--) {
        if (label[k] === '0') {
          zeros++;
        } else {
          break;
        }
      }

      nRounds += zeros;
      value += step;
    }

    if (nRounds > maxRounds) {
      maxRounds = nRounds;
      maxRoundsSteps = steps;
    }
  }
  return maxRoundsSteps;
}

/**
 * Считает оптимальное количество знаков после запятой для легенды графика
 *
 * @param min - минимальное значение легенды
 * @param max - максимальное значение легенды
 * @param legendSteps - количество шагов легенды
 */
export function getLegendDecimals(min: number, max: number, legendSteps = 5) {
  let step = Math.abs((max - min) / legendSteps);
  const log10 = Math.floor(Math.log10(step));

  if (step === 0) {
    return 0;
  }

  if (log10 < 0) {
    return Math.abs(log10);
  }

  return 0;
}
