/**
 * Возвращает случайное целое число между ceil(min) и floor(max), не включая max
 * @param min {number}
 * @param max {number}
 * @returns {number}
 */
export function getRandomInteger(min = 0, max = 100) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min);
}
