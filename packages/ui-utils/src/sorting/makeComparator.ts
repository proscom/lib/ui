/**
 * Базовый компаратор. Сравнивает, используя операторы `<` и `>`
 * @param dir - направление сравнения (1 для asc, -1 для desc)
 */
export const compareSimple = <T>(dir = 1) => (a: T, b: T) =>
  a < b ? -dir : a > b ? dir : 0;

export type CompareFn<T> = (a: T, b: T) => number;
export type FieldAccessor<Obj, Field> = (fieldValue: Obj) => Field;

/**
 * Создает функцию сравнения по полям объекта
 * @param compFn - компаратор
 * @param fieldFns - последовательность полей
 * @example
 *  const fn = makeComparator(compareSimple(), x => x.a, x => x.b)
 *  const sorted = [{a:2,b:1},{a:1,b:2},{a:1,b:1}].sort(fn);
 *  sorted -> [{a:1,b:1},{a:1,b:2},{a:2,b:1}]
 */
export function makeComparator<ObjectType, CompareType = ObjectType>(
  compFn: CompareFn<CompareType>,
  ...fieldFns: readonly (FieldAccessor<ObjectType, CompareType>)[]
) {
  return (a: ObjectType, b: ObjectType) => {
    for (const fieldFn of fieldFns) {
      const compRes = compFn(fieldFn(a), fieldFn(b));
      if (compRes !== 0) return compRes;
    }
    return 0;
  };
}

/**
 * Создает функцию сравнения по последовательности полей объекта.
 * По сравнению с makeComparator позволяет использовать разные функции
 * сравнения для разных полей
 *
 * @param steps - массив, элементами которого являются шаги сортировки
 *  - массивы из компараторов и подпоследовательности полей
 * @example
 *  const fn = makeMultiComparator([compareSimple(), x => x.a], [compareSimple(-1), x => x.b])
 *  const sorted = [{a:2,b:1},{a:1,b:2},{a:1,b:1}].sort(fn);
 *  sorted -> [{a:1,b:2},{a:1,b:1},{a:2,b:1}]
 */
export function makeMultiComparator<ObjectType, CompareTypes extends any[]>(
  ...steps: {
    [CompareType in keyof CompareTypes]: readonly [
      CompareFn<CompareType>,
      ...(FieldAccessor<ObjectType, CompareType>)[]
    ];
  }
) {
  return (a: ObjectType, b: ObjectType) => {
    for (const step of steps) {
      const [ compFn, ...fieldFns ] = step;
      for (const fieldFn of fieldFns) {
        const compRes = compFn(fieldFn(a), fieldFn(b));
        if (compRes !== 0) return compRes;
      }
    }
    return 0;
  };
}
