/**
 * Компаратор для численных строк.
 * Если одна из строк не является числом, то она смещается в конец
 * @param dir - направление сравнения (1 для asc, -1 для desc)
 */
export const compareNumericStrings = (dir = 1) => (a: string, b: string) => {
  const aNumber = +a;
  const bNumber = +b;
  const aNaN = isNaN(aNumber);
  const bNaN = isNaN(bNumber);
  if (aNaN && bNaN) {
    return a < b ? -dir : a > b ? dir : 0;
  }
  if (aNaN) return 1;
  if (bNaN) return -1;
  return aNumber < bNumber ? -dir : aNumber > bNumber ? dir : 0;
};
