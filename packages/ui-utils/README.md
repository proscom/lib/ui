# `@proscom/ui-utils`

Набор утилит

## Установка

```
yarn add @proscom/ui-utils
//
npm install --save @proscom/ui-utils
```

## Состав

### Массивы

- [findMinInArray](./src/array/findMinInArray.ts)
- [findMinIndex](./src/array/findMinIndex.ts)

### Асинхронщина

- [eachOfLimit](./src/async/async.ts)
- [mapLimit](./src/async/async.ts)
- [concatLimit](./src/async/async.ts)
- [awaitChunkedPromises](./src/async/async.ts)
- [delayPromise](./src/async/delayPromise.ts)
- [singleton](./src/async/singleton.ts)

### Хелперы

- [define](./src/helpers/define.ts)
- [EnumHelpers](./src/helpers/EnumHelpers.ts)
- [SingleTimeoutManager](./src/helpers/SingleTimeoutManager.ts)

### Работа с числами

- Легенда для графиков
  - [roundToRound](./src/number/chartLegend.ts)
  - [getRoundLegendSteps](./src/number/chartLegend.ts)
  - [getLegendDecimals](./src/number/chartLegend.ts)
- Показатели для графиков
  - [getIndexDecimals](./src/number/indexes.ts)
  - [formatIndexValue](./src/number/indexes.ts)
  - [formatIndexValueUnit](./src/number/indexes.ts)
- Форматирование
  - [formatFileSize](./src/number/formatFileSize.ts)
  - [formatNumber](./src/number/formatNumber.ts)
  - [formatValueUnit](./src/number/formatNumber.ts)
  - [formatPercent](./src/number/formatNumber.ts)
  - [declineUnit](./src/number/formatNumber.ts)
- Остальное
  - [findMinWrapped](./src/number/findMinWrapped.ts)
  - [getRandomInteger](./src/number/random.ts)
  - [isValidNumber](./src/number/tryNumber.ts)
  - [tryNumber](./src/number/tryNumber.ts)

### Работа с объектами

- [applyDefaults](./src/object/applyDefaults.ts)
- [areArraysShallowEqual](./src/object/areArraysShallowEqual.ts)
- [cleanArray](./src/object/cleanArray.ts)
- [cleanObject](./src/object/cleanObject.ts)
- [createFilterSearch](./src/object/filterSearch.ts)
- [filterSearch](./src/object/filterSearch.ts)
- [insertPaginatedSlice](./src/object/insertPaginatedSlice.ts)
- [tryParseJson](./src/object/tryParseJson.ts)

### Сортировка

- [makeComparator](./src/sorting/makeComparator.ts)
- [makeMultiComparator](./src/sorting/makeComparator.ts)
- [compareSimple](./src/sorting/makeComparator.ts)
- [compareNumericStrings](./src/sorting/compareNumericStrings.ts)

### Работа со строками

- [declinedText](./src/string/declinedText.ts)
- [joinNonEmpty](./src/string/joinNonEmpty.ts)
- [makeOptionator](./src/string/makeOptionator.ts)
- [shortenText](./src/string/shortenText.ts)

### Работа с ошибками

- [fixErrorPrototype](./src/error.ts)
- [getJsonError](./src/error.ts)
- [tryGetJsonError](./src/error.ts)
- [CustomError](./src/error.ts)
- [WrapperError](./src/error.ts)

### Типы TypeScript

- [Optional](./src/types.ts)
- [Extends](./src/types.ts)

### Работа с адресной строкой

- [getParentUrl](./src/url.ts)
- [parseQuerySimple](./src/url.ts)
- [encodeQuerySimple](./src/url.ts)
- [parseQueryArray](./src/url.ts)
- [encodeQueryArray](./src/url.ts)

### Константы для некоторых Unicode-символов

| Константа | utf8   | Символ | Описание               |
| --------- | ------ | ------ | ---------------------- |
| LAQUO     | \u00AB | «      | Левые двойные кавычки  |
| RAQUO     | \u00BB | »      | Правые двойные кавычки |
| EMDASH    | \u2014 | —      | Длинное тире           |
| THINSP    | \u2009 |        | Тонкий пробел          |
| QUARTERSP | \u2005 |        | Четвертной пробел      |
| THIRDSP   | \u2004 |        | Третичный пробел       |
| ENSP      | \u2002 |        | N-пробел               |
| EMSP      | \u2001 |        | M-пробел               |
| NBSP      | \u00A0 |        | Неразрывный пробел     |
| HELLIP    | \u2026 | …      | Многоточие             |
| LSAQUO    | \u2039 | ‹      | Левая кавычка          |
| RSAQUO    | \u203A | ›      | Правая кавычка         |
| RUBLE     | \u20BD | ₽      | Символ рубля           |
| LEQ       | \u2264 | ≤      | Меньше или равно       |
| NEQ       | \u2260 | ≠      | Не равно               |
| GEQ       | \u2265 | ≥      | Больше или равно       |
