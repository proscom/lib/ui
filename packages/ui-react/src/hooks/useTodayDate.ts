import { useEffect, useState } from 'react';

/**
 * Возвращает текущую дату. Компонент обновится в полночь
 */
export function useTodayDate(): Date {
  const [date, setDate] = useState(new Date());

  useEffect(() => {
    const today = new Date();
    const endOfDay = new Date(today);
    endOfDay.setHours(23, 59, 59, 999);
    const untilNextDay = endOfDay.getTime() - date.getTime() + 1;

    const timeout = setTimeout(() => {
      setDate(new Date());
    }, untilNextDay);

    return () => clearTimeout(timeout);
  }, [date]);

  return date;
}
