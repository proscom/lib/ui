import { useRef } from 'react';

/**
 * Прокидывает пропы в реф. Полезно для замыкания последних значений в колбеки
 * @param props - пропы, которые нужно прокинуть
 *
 * @example
 *  function Component({ value }) {
 *    const latest = usePropsRef({ value });
 *    const callback = useCallback(() => console.log(latest.value));
 *    // ...
 *  }
 */
export function usePropsRef<T>(props: T): T {
  const ref = useRef<T>({} as T);
  Object.assign(ref.current, props);
  return ref.current;
}
