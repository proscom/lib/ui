import { useCallback } from 'react';
import { useImmediateState } from './useImmediateState';

/**
 * Хук позволяет реализовывать компоненты, которые могут быть как
 * контролируемые, так и неконтролируемые, аналогично стандартному `input` в React.
 *
 * Контролируемость определяется наличием пропа, задающего значение.
 *
 * @param prop - проп, задающий значение при контролируемом использовании
 * @param defaultProp - проп, задающий значение по умолчанию при неконтролируемом использовании
 * @param defaultValue - значение по умолчанию для случаев если ни prop ни defaultProp не заданы
 * @param onChange - колбек, который вызывается при изменении значения
 *
 * @return tuple - кортеж из трех значений:
 *  - state - текущее значение, которое может быть использовано вместо prop
 *  - handleChange - колбек, который надо вызывать при необходимости изменить значение изнутри компонента
 *  - isControlled - флаг контролируемости компонента
 *
 * @example
 * function MyInput({ value: valueProp, defaultValue, onChange }) {
 *   const [value, handleChange, isControlled] = useControlledState(valueProp, defaultValue, '', onChange);
 *   return <input value={value} onChange={handleChange} />;
 * }
 */
export function useControlledState<T>(
  prop: T | undefined,
  defaultProp: T | undefined,
  defaultValue: T,
  onChange: undefined | ((value: T) => void)
) {
  const targetPropValue = prop ?? defaultProp ?? defaultValue;
  const [state, setState] = useImmediateState(targetPropValue);
  const isControlled = prop !== undefined;

  const handleChange = useCallback(
    (value: T) => {
      if (!isControlled) {
        setState(value);
      }
      onChange?.(value);
    },
    [isControlled, onChange]
  );

  return [state, handleChange, isControlled] as const;
}
