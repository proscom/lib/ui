import { SetStateAction, useCallback, useRef, useState } from 'react';

/**
 * Хук, который позволяет при зависимости стейта от пропов
 * получить обновленное значение стейта в том же рендере при изменении пропов.
 *
 * @param targetValue - целевое значение, вычисленное от пропов
 *
 * @return tuple - кортеж из двух значений:
 *  - state - текущее значение состояния
 *  - setState - функция изменения состояния
 *
 * @example
 * function Component({ defaultValue }) {
 *   const [value, setValue] = useImmediateState(defaultValue);
 *   return <button onClick={setValue(v => v + 1)}>{value}</button>;
 * }
 */
export function useImmediateState<T>(targetValue: T) {
  // Фейковое состояние, чтобы перерендерить компонент при изменении стейта
  const [, rerender] = useState(1);

  // Эмулируем useState
  const stateRef = useRef<T>(targetValue);
  const setState = useCallback((value: SetStateAction<T>) => {
    stateRef.current =
      typeof value === 'function'
        ? (value as (v: T) => T)(stateRef.current)
        : value;
    rerender((s) => s + 1);
  }, []);

  // Отслеживаем изменения targetValue и меняем стейт, если он меняется
  const previousValue = useRef(targetValue);
  if (previousValue.current !== targetValue) {
    stateRef.current = targetValue;
    previousValue.current = targetValue;
  }

  return [stateRef.current, setState] as const;
}
