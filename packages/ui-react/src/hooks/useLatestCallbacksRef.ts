import { usePropsRef } from "./usePropsRef";
import { useFactoryRef } from "./useFactoryRef";

export type ICallbackObject = {
  [key: string]: (...args: any[]) => any;
}

/**
 * Позволяет обернуть меняющиеся колбеки в неменяющиеся с помощью рефа
 * @param props - объект, значениями которого являются колбеки
 * @returns объект той же структуры, но в котором функции ссылочно постоянны
 * @example
 *  const t = new Date();
 *  const funcs = useLatestCallbacksRef({ onClick: () => console.log(t) });
 *  funcs.onClick -> постоянная функция, которая вызывает последний переданный в onClick колбек
 */
export function useLatestCallbacksRef<T extends ICallbackObject>(props: T): T {
  const propsRef = usePropsRef<T>(props);
  return useFactoryRef<T>(() => {
    const result = {};
    for (const key of Object.keys(props)) {
      result[key] = (...args) => propsRef[key](...args);
    }
    return result as T;
  });
}
