import { useEffect, useState } from 'react';
import { usePropsRef } from './usePropsRef';

export interface UseCountdownOptions {
  /**
   * Интервал в мс, не чаще которого будет тикать счетчик
   */
  interval?: number;
  /**
   * Функция которая вызывается при каждом тике (не чаще чем interval мс)
   * @param timeLeft - время до наступления заданной даты
   */
  onTick?: (timeLeft: number) => void;
  /**
   * Функция которая вызывается при наступлении заданной даты
   */
  onFinish?: () => void;
}

export interface UseCountdownStateOptions extends UseCountdownOptions {
  /**
   * Текущая дата для задания первоначального стейта.
   * Если не передано, то используется new Date()
   */
  now?: Date;
}

/**
 * Позволяет сделать обратный отсчет до заданной даты.
 * Не вызывает перерендер компонента при тике.
 *
 * @param target - заданная дата
 * @param options - опции хука
 */
export function useCountdown(
  target: Date | null,
  options: UseCountdownOptions
) {
  const propsRef = usePropsRef(options);

  useEffect(() => {
    if (!target) return;

    const intervalId = setInterval(() => {
      const now = new Date();
      const timeLeft = target.getTime() - now.getTime();
      if (timeLeft < 0) {
        clearInterval(intervalId);
        propsRef.onTick?.(timeLeft);
        propsRef.onFinish?.();
      } else {
        propsRef.onTick?.(timeLeft);
      }
    }, options.interval ?? 1000);

    return () => {
      clearInterval(intervalId);
    };
  }, [propsRef, target, options.interval]);
}

/**
 * Позволяет сделать обратный отсчет до заданной даты.
 * Вызывает перерендер компонента при тике.
 *
 * @param target - заданная дата
 * @param options - опции хука
 * @returns время в мс до наступления заданной даты
 */
export function useCountdownState(
  target: Date | null,
  options: UseCountdownStateOptions = {}
) {
  const renderDate = options.now || new Date();
  const [timeLeft, setTimeLeft] = useState<number | null>(
    target ? target.getTime() - renderDate.getTime() : null
  );

  useCountdown(target, {
    ...options,
    onTick: (timeLeft) => {
      if (timeLeft > 0) {
        setTimeLeft(timeLeft);
      }
      options?.onTick?.(timeLeft);
    },
    onFinish: () => {
      setTimeLeft(0);
      options?.onFinish?.();
    }
  });

  return timeLeft;
}
