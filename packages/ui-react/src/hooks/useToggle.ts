import React, { useState } from 'react';
import { useFactoryRef } from './useFactoryRef';

export type TUseToggle = {
  change: React.Dispatch<React.SetStateAction<boolean>>;
  set: () => void;
  toggle: () => void;
  unset: () => void;
  value: boolean;
};

/**
 * Создаёт булев стейт который можно переключать различными способами
 * @example
 *  function MyComponent() {
 *    const open = useToggle();
 *    return <div>
 *      <button onClick={open.toggle}>Toggle</button>
 *      {open.value && <div>
 *        <span>Modal</span>
 *        <button onClick={open.unset}>Close</button>
 *      </div>}
 *    </div>;
 *  }
 */
export function useToggle(): TUseToggle {
  const [value, change] = useState(false);

  const funcsRef = useFactoryRef(() => ({
    set: () => {
      change(true);
    },
    unset: () => {
      change(false);
    },
    toggle: () => {
      change(s => !s);
    }
  }));

  return {
    ...funcsRef,
    value,
    change
  };
}
