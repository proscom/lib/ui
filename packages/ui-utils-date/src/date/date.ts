import {
  endOfWeek,
  format,
  isBefore,
  isEqual,
  isSameWeek,
  isValid,
  parseISO,
  startOfWeek,
} from 'date-fns';
import ruLocale from 'date-fns/locale/ru';
import { EMDASH } from '@proscom/ui-utils';
import nomRuLocale from './fnsNomRuLocale';

export type FormatOptions = Parameters<typeof format>[2];
export type DateOrTimestamp = number | Date;

export const DATE_FORMAT_RUSSIAN = 'dd.MM.yyyy';
export const DATETIME_FORMAT_RUSSIAN = 'dd.MM.yyyy HH:mm:ss';
export const DATE_FORMAT_ISO_DATE = 'yyyy-MM-dd';
export const DATETIME_FORMAT_ISO = 'yyyy-MM-ddTHH:mm:ssXXX';
export const DATE_FORMAT_MONTH = 'LLLL yyyy';

/**
 * Переформатирует дату из ISO строки в заданный формат
 * @param isoDate - строка, содержащая дату в формате ISO
 * @param target - формат
 * @param defaultValue - значение по-умолчанию
 */
export function reformatDate(isoDate: string|null|undefined, target = DATE_FORMAT_RUSSIAN, defaultValue = EMDASH) {
  if (!isoDate) return defaultValue;
  const date = parseISO(isoDate);
  if (!date) return defaultValue;
  return format(date, target);
}

/**
 * Возвращает первый день недели с учетом того, что неделя начинается в понедельник
 * @param date - дата
 */
export function lStartOfWeek(date: DateOrTimestamp) {
  return startOfWeek(date, { weekStartsOn: 1 });
}

/**
 * Возвращает последний день недели с учетом того, что неделя начинается в понедельник
 * @param date - дата
 */
export function lEndOfWeek(date: DateOrTimestamp) {
  return endOfWeek(date, { weekStartsOn: 1 });
}

/**
 * Проверяет, что две даты находятся в рамках одной недели с учетом того,
 * что неделя начинается в понедельник
 * @param dateLeft - первая дата
 * @param dateRight - вторая дата
 */
export function lIsSameWeek(dateLeft: DateOrTimestamp, dateRight: DateOrTimestamp) {
  return isSameWeek(dateLeft, dateRight, { weekStartsOn: 1 });
}

/**
 * Форматирует дату с использованием русской локали
 * @param date - дата
 * @param formatString - формат
 * @param options - дополнительные опции
 */
export function lFormat(
  date: DateOrTimestamp,
  formatString: string,
  options: FormatOptions = {}
) {
  return format(date, formatString, { ...options, locale: ruLocale });
}

/**
 * Форматирует дату с использованием русской номинативной локали
 * (в именительном падеже)
 *
 * @param date - дата
 * @param formatString - формат
 * @param options - дополнительные опции
 */
export function lnFormat(
  date: DateOrTimestamp,
  formatString: string,
  options: FormatOptions = {}
) {
  return format(date, formatString, { ...options, locale: nomRuLocale });
}

/**
 * Форматирует дату в формат ISO (без времени)
 * @param date - дата
 */
export function formatDateIso(date: DateOrTimestamp) {
  return format(date, DATE_FORMAT_ISO_DATE);
}

/**
 * Проверяет, что левая дата не больше правой
 * @param dateLeft - левая дата
 * @param dateRight - правая дата
 */
export function isBeforeOrEqual(
  dateLeft: DateOrTimestamp,
  dateRight: DateOrTimestamp
) {
  return isBefore(dateLeft, dateRight) || isEqual(dateLeft, dateRight);
}

/**
 * Пробует распарсить строку в формате ISO.
 * Если получается, то возвращает получившийся объект даты.
 * Иначе возвращает null
 *
 * @param string - строка, содержащая дату в формате ISO
 */
export function tryParseIso(string?: string) {
  if (!string) return null;
  const date = parseISO(string);
  if (isValid(date)) {
    return date;
  }
  return null;
}
