# `@proscom/ui-utils-date`

Набор утилит для работы с датами

## Установка

```
yarn add @proscom/ui-utils-date date-fns
//
npm install --save @proscom/ui-utils-date date-fns
```

## Состав

### Константы форматов

- [DATE_FORMAT_RUSSIAN](./src/date/date.ts)
- [DATETIME_FORMAT_RUSSIAN](./src/date/date.ts)
- [DATE_FORMAT_ISO_DATE](./src/date/date.ts)
- [DATETIME_FORMAT_ISO](./src/date/date.ts)
- [DATE_FORMAT_MONTH](./src/date/date.ts)

### Форматирование

- [reformatDate](./src/date/date.ts)
- [formatDateIso](./src/date/date.ts)
- [tryParseIso](./src/date/date.ts)

### Дополнительные утилиты

- [isBeforeOrEqual](./src/date/date.ts)

### Функции для русской локали

- [lStartOfWeek](./src/date/date.ts)
- [lEndOfWeek](./src/date/date.ts)
- [lIsSameWeek](./src/date/date.ts)
- [lFormat](./src/date/date.ts)
- [lnFormat](./src/date/date.ts)

- [nomRuLocale](./src/date/fnsNomRuLocale.ts)

### Сортировка

- [compareDateStrings](./src/sorting/compareDateStrings.ts)
