# `@proscom/ui`

Коллекция утилитарных пакетов

## Пакеты

[@proscom/ui-react](./packages/ui-react)

[@proscom/ui-utils](./packages/ui-utils)

[@proscom/ui-utils-date](./packages/ui-utils-date)
